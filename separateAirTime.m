%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is a part of a working example for the analysis of 
% temperature depth profiles in a water body. 
%
% Copyright (c) 2023, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, in 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dataAir, dataWater] = separateAirTime(data, limit)

idxAir = data.T < limit;
idxWater = logical(1 - idxAir);
dataAir = data(idxAir,:);
dataWater = data(idxWater,:);

dataWater.Depth = dataWater.Depth - dataWater.Depth(1);