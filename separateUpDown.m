%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is a part of a working example for the analysis of 
% temperature depth profiles in a water body. 
%
% Copyright (c) 2023, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, in 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function [dataAb, dataAuf] = separateUpDown(data, idAbEnd, idAufStart)

idAb = logical([ones(idAbEnd,1); zeros(length(data.P)-idAbEnd,1)]);
idAuf = logical([zeros(idAufStart-1,1); ones(length(data.P)-idAufStart+1,1)]);

dataAb = data(idAb,:);
dataAuf = data(idAuf,:);