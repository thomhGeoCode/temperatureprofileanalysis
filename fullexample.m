%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is a working example for the analysis of temperature depth
% profiles in a water body. 
% This script requires the Matlab Signal processing toolbox.
%
% Copyright (c) 2023, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, in 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc

%% Import and calculate depth
data = importfileSensor('exampleData.txt', 3);

%% Separate the air time based on temperature value
limitAirTemp = 25.9;

[dataAir, dataWater] = separateAirTime(data, limitAirTemp);

clear limitAirTemp
% Remove Air data and the full data set because it is not used in the 
% analysis
clear dataAir data20211126
%% Visualize the full profile
figure(); 
plot(-dataWater.Depth);
xlabel('data points (-)', 'FontSize', 8)
ylabel('depth (m)', 'FontSize', 8)
ylim([-800 0])
%% Separating descend and ascend
% Points read from figure
[dataDown, dataUp] = separateUpDown(dataWater, 6907, 7049);

%% Determine Rate of descend and ascend
RateDescend = (dataDown.Depth(1)-dataDown.Depth(end)) / ...
  minutes(dataDown.time(1)-dataDown.time(end))

RateAscend = -(dataUp.Depth(1)-dataUp.Depth(end)) / ...
  minutes(dataUp.time(1)-dataUp.time(end))

clear RateDescend RateAscend
%% Determine noise in stationary period
% Points read from figure
ids = [6908 7048];

figure()
plot(dataWater.T(ids(1):ids(2)) - ...
  mean(dataWater.T(ids(1):ids(2))));
xlabel('data points (-)', 'FontSize', 8)
ylabel('temperature (°C)', 'FontSize', 8)
box on

stationaryStd = std(dataWater.T(ids(1):ids(2)))

clear ids stationaryStd

%% identify outliers
Tdown51 = ~isoutlier(dataDown.T,'movmean',51);
Tup51 = ~isoutlier(dataUp.T,'movmean',51);

%% smoothing of filtered data
figure()
hold on
plot(movmean(dataDown.T(Tdown51),51), -dataDown.Depth(Tdown51), '-k'); 
plot(movmean(dataDown.T(Tdown51),51)+3.0.*movstd(dataDown.T(Tdown51),51),...
  -dataDown.Depth(Tdown51), '-r'); 
plot(movmean(dataDown.T(Tdown51),51)-3.0.*movstd(dataDown.T(Tdown51),51),...
  -dataDown.Depth(Tdown51), '-r');

plot(movmean(dataUp.T(Tup51),17), -dataUp.Depth(Tup51), '-b'); 
plot(movmean(dataUp.T(Tup51),17)+3.0.*movstd(dataUp.T(Tup51),17), ...
  -dataUp.Depth(Tup51), '-g'); 
plot(movmean(dataUp.T(Tup51),17)-3.0.*movstd(dataUp.T(Tup51),17), ...
  -dataUp.Depth(Tup51), '-g');
hold off
xlabel('temperature (°C)')
ylabel('distance to water surface (m)')
ylim([-800 0])

%% Interpolating the downwards movement to the same height values than the 
% upward profile because both profiles have different number of measurement
% points and different heights. Substraction requires similar heights.
[C, ia, ic] = unique(dataDown.Depth(Tdown51));
[D, iaa, ica] = unique(dataUp.Depth(Tup51));
xq = interp1(-unique(dataDown.Depth(Tdown51)),dataDown.T(ia),-unique(dataUp.Depth(Tup51)));

figure()
tiledlayout(1,2);
nexttile
hold on
plot(xq, -unique(dataUp.Depth(Tup51)),'-k')
plot(dataUp.T(iaa),-unique(dataUp.Depth(Tup51)),'-b')
plot(movmean(dataDown.T(Tdown51),51)+3.0.*movstd(dataDown.T(Tdown51),51), -dataDown.Depth(Tdown51), '-r'); 
plot(movmean(dataDown.T(Tdown51),51)-3.0.*movstd(dataDown.T(Tdown51),51), -dataDown.Depth(Tdown51), '-r');
hold off
ylim([-800 0])
xlabel('temperature (°C)', 'FontSize', 8)
ylabel('distance to water surface (m)', 'FontSize', 8)
legend('descend','ascend','+/-3 \cdot \sigma', 'FontSize', 8)
text(25.85,0,'a)', 'FontSize', 8)
box on

nexttile
hold on
plot(xq-dataUp.T(iaa), -unique(dataUp.Depth(Tup51)),'-k')
plot(3.0.*movstd(dataDown.T(Tdown51),51), -dataDown.Depth(Tdown51), '-r'); 
plot(-3.0.*movstd(dataDown.T(Tdown51),51), -dataDown.Depth(Tdown51), '-r');
hold off
ylim([-800 0])
xlabel('temperature difference (°C)', 'FontSize', 8)
text(-0.07,0,'b)', 'FontSize', 8)
box on

clear C D f ia ic ica Tup51
%% Calculate percentage of data within the different standard deviations
X = xq-dataUp.T(iaa) < 3.0.*movstd(dataDown.T(iaa),31);
disp(['Percentage of data within 3 \sigma range: ', ...
  num2str(nnz(X)/numel(xq)*100),'%'])

X = xq-dataUp.T(iaa) < 2.0.*movstd(dataDown.T(iaa),31);
disp(['Percentage of data within 2 \sigma range: ', ...
  num2str(nnz(X)/numel(xq)*100),'%'])

X = xq-dataUp.T(iaa) < movstd(dataDown.T(iaa),31);
disp(['Percentage of data within 1 \sigma range: ', ...
  num2str(nnz(X)/numel(xq)*100),'%'])

disp(['\sigma= ', num2str(mean(movstd(dataDown.T(iaa),31))),'°C'])

%% Setting parameters
numSec = 15;
order = 3;
windowLength = 31;
slopeThresh = 0.001;
stdThresh = 1000;
meanThresh = 0.05;

%% Smoothing/Filtering focusing on the more precise descend measurement
smooth  = sgolayfilt(dataDown.T(Tdown51), order, windowLength);

%% Find Changingpoints based on slope
iptS = findchangepts(smooth, 'Statistic','linear','MinThreshold', ...
  slopeThresh);

%% Linear interpolation per segment and chi^2 calculation
ListSegmS = [1; iptS; length(dataDown.T)];

for i=1:length(ListSegmS)-1
  x = dataDown.Depth(ListSegmS(i):ListSegmS(i+1));
  y = dataDown.T(ListSegmS(i):ListSegmS(i+1));

  P(i,:) = polyfit(x,y,1);
  yfit = polyval(P(i,:),x);
  yresid = y - yfit;
  chisqd(i) = sum(yresid.^2./yfit)./(length(yresid)-1);
end

PList = ones(length(dataDown.T),1);
chisqdList = ones(length(dataDown.T),1);
for i=1:length(ListSegmS)-1
  PList(ListSegmS(i):ListSegmS(i+1)) = P(i,1);
  chisqdList(ListSegmS(i):ListSegmS(i+1)) = chisqd(i);
end

%% Find Changingpoints based on noise
iptN = findchangepts(smooth, 'Statistic','std','MinThreshold', stdThresh);

%% Fing Changingpoints based on mean
iptM = findchangepts(smooth, 'Statistic','mean','MinThreshold',meanThresh);

%% Calculate mean in each segment
ListSegmM = [1; iptM; length(dataDown.T)];

MeanSeg = ones(length(ListSegmM),1);
for i=1:length(ListSegmM)-1
  MeanSeg(i) = mean(dataDown.T(ListSegmM(i):ListSegmM(i+1)));
end

MList = ones(length(dataDown.T),1);
for i=1:length(ListSegmM)-1
  MList(ListSegmM(i):ListSegmM(i+1)) = MeanSeg(i);
end

%% Calculate noise in each segment
ListSegmN = [1; iptN; length(dataDown.T)];

StdSeg = ones(length(ListSegmN),1);
for i=1:length(ListSegmN)-1
  StdSeg(i) = std(dataDown.T(ListSegmN(i):ListSegmN(i+1)));
end

StdList04 = ones(length(dataDown.T),1);
for i=1:length(ListSegmN)-1
  StdList04(ListSegmN(i):ListSegmN(i+1)) = StdSeg(i);
end
%%
figure()
t=tiledlayout(1,4);
nexttile
plot(smooth, -dataDown.Depth(Tdown51),'-k')
ylabel('distance to water surface (m)')
xlabel('temperature (°C)')
text(25.87,0,"a)")
box on

nexttile
hold on
plot(PList, -dataDown.Depth,'-k')
plot([0 0],[-800 0],'-r')
plot([mean(PList) mean(PList)],[-800 0],'--k')
hold off
xlim([-0.001 0.005])
xlabel('slope (°C/m)')
text(-2.0e-3,0,"b)")
box on

nexttile
hold on
plot(chisqdList,-dataDown.Depth, '-k')
plot([mean(chisqdList) mean(chisqdList)],[-800 0],'--k')
hold off
xlim([0 2e-6])
xlabel('\chi^2 (-)')
text(-3.7e-7,0,"c)")
box on

nexttile
hold on
plot(StdList04,-dataDown.Depth, '-k')
plot([mean(StdList04) mean(StdList04)],[-800 0],'--k')
hold off
xlabel('\sigma (°C)')
text(-0.004,0,"d)")
box on